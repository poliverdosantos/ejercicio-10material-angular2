import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario.service';
import { Router } from '@angular/router';
import { ListaValoresI } from '../interface/lista_valores';
import { Content } from '@angular/compiler/src/render3/r3_ast';
import * as jsPDF from 'jspdf';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  //valores:ListaValoresI[] =[]


  valores: any
  valoress: any
  constructor(private servicioUsuario: UsuarioService, private router: Router) { }

  ngOnInit(): void {
    this.servicioUsuario.obtenerValor().subscribe({
      next: user => {
        console.log(user);
        this.valores = user['data'][1];
        console.log(user);
       
      }
    })
  }
  imprimirLista(){
    const doc = new jsPDF();
    doc.fromHTML(document.getElementsById('extraer'),10,10)
    doc.save('Lista')
  }
 
}
