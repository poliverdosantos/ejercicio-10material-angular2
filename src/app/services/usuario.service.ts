import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ListaValoresI } from '../components/interface/lista_valores';


@Injectable({
  providedIn: 'root'
})

export class UsuarioService {
  url='https://reqres.in/api/unknown'

  constructor(public  http:HttpClient) { }


  obtenerValor(): Observable<any>{
    return this.http.get<any>(this.url);
  }
}
